package com.example.mobilephone1.ext

fun String?.validHttp(): String? {
    return this?.let {
        if (!this.contains("https://") && !this.contains("http://")) {
            "https://$this"
        } else {
            this
        }
    }
}