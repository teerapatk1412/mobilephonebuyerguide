package com.example.mobilephone1.ext

import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.showImage(url: String?){
    Picasso.get().load(url.validHttp()).into(this)
}