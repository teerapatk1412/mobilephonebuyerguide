package com.example.mobilephone1.service

import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.model.MobilePictureModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface MobileApiService {
    @GET("api/mobiles/")
    fun getMobile(): Call<List<MobileModel>>

    @GET("api/mobiles/{mobile_id}/images/")
    fun getPictureMobile(@Path("mobile_id") mobileId: Int): Call<List<MobilePictureModel>>
}