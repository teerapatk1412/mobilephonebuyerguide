package com.example.mobilephone1.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MobileManager {
    companion object {
        const val BASE_MOBILE_API = "https://scb-test-mobile.herokuapp.com/"
    }
    fun createService(): MobileApiService =
         Retrofit.Builder()
             .baseUrl(BASE_MOBILE_API)
             .addConverterFactory(GsonConverterFactory.create())
             .build()
             .run { create(MobileApiService::class.java) }

}