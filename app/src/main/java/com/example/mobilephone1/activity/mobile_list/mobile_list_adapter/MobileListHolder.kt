package com.example.mobilephone1.activity.mobile_list.mobile_list_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilephone1.R
import com.example.mobilephone1.activity.mobile_list.MobileItemClickListener
import com.example.mobilephone1.ext.showImage
import com.example.mobilephone1.model.MobileModel

class MobileListHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.mobilelistcard, parent, false)
) {

    private val mobileName: TextView = itemView.findViewById(R.id.mobileName)
    private val mobileDes: TextView = itemView.findViewById(R.id.mobileDes)
    private val mobilePrice: TextView = itemView.findViewById(R.id.mobilePrice)
    private val mobileRating: TextView = itemView.findViewById(R.id.mobileRating)
    private val mobilePicture: ImageView = itemView.findViewById(R.id.mobilePic)
    private var imageButton: ImageView = itemView.findViewById(R.id.imageButton)

    fun bind(model: MobileModel, listener: MobileItemClickListener) {

        itemView.setOnClickListener {
            listener.onItemClick(model)
        }

        if (model.favorite) {
            imageButton.setImageResource(R.drawable.ic_favorite_black_24dp)
        } else {
            imageButton.setImageResource(R.drawable.ic_favorite_border_black_24dp)
        }

        imageButton.setOnClickListener {
            listener.onFavoriteClick(model)
        }

        mobileName.text = model.name
        mobileDes.text = model.description
        mobilePrice.text = model.price
        mobileRating.text = model.rating
        mobilePicture.showImage(model.thumbImageURL)

    }
}
