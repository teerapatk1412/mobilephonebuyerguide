package com.example.mobilephone1.activity.mobile_detail.mobile_detail_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilephone1.R
import com.example.mobilephone1.ext.showImage
import com.example.mobilephone1.model.MobilePictureModel

class MobileDetailHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.mobile_detail_card, parent, false)
) {

    private val mobileDetailPic: ImageView = itemView.findViewById(R.id.imgMobile)

    fun bind(model: MobilePictureModel) {
        mobileDetailPic.showImage(model.url)
    }


}