package com.example.mobilephone1.activity.mobile_detail.mobile_detail_adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilephone1.model.MobilePictureModel

class MobileDetailAdapter : RecyclerView.Adapter<MobileDetailHolder>() {

    private var mobilePictureModel: List<MobilePictureModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MobileDetailHolder {
        return MobileDetailHolder(parent)
    }

    override fun getItemCount(): Int = mobilePictureModel.count()

    override fun onBindViewHolder(holder: MobileDetailHolder, position: Int) {
        holder.bind(mobilePictureModel[position])
    }
    fun submit(list: List<MobilePictureModel>){
        mobilePictureModel = list
        notifyDataSetChanged()
    }

}