package com.example.mobilephone1.activity.mobile_detail

import com.example.mobilephone1.model.MobilePictureModel

interface MobileDetailInterface {
    fun setMobile(mobileName : String,
                  mobileBrand : String,
                  mobileDes : String
                  )
//    fun setPictureMobile(mobileModelPic: List<MobilePictureModel>)
    fun setMobilePic(mobilePic : List<MobilePictureModel>)

    fun showMessage(msg: String)
}