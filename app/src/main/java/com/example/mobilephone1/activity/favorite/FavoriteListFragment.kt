package com.example.mobilephone1.activity.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mobilephone1.R
import com.example.mobilephone1.activity.favorite.favorite_adapter.FavoriteListAdapter
import com.example.mobilephone1.model.MobileModel
import kotlinx.android.synthetic.main.favoritelist.*


class FavoriteListFragment : Fragment(), FavoriteListInterface {

    private lateinit var favoriteListAdapter: FavoriteListAdapter
    private val favoritePresenter = FavoriteListPresenter(this)

    override fun setFavorite(mobileModelList: List<MobileModel>) {

        favoriteListAdapter.submit(mobileModelList)

    }

    companion object {
        fun newInstance(): FavoriteListFragment = FavoriteListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.favoritelist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoriteListAdapter = FavoriteListAdapter()
        rvFavoriteList.adapter = favoriteListAdapter
        rvFavoriteList.layoutManager = LinearLayoutManager(context)

    }

    fun checkDataToFavorite(model: MobileModel) {
        favoritePresenter.setData(model)

    }

    fun favoriteSortingLowToHigh() {
        favoritePresenter.favoriteSortLowToHigh()
    }

    fun favoriteSortingHighToLow() {
        favoritePresenter.favoriteSortHighToLow()
    }

    fun favoriteSortingRating() {
        favoritePresenter.favoriteSortRating()
    }
}