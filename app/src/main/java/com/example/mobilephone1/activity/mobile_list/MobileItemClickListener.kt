package com.example.mobilephone1.activity.mobile_list

import com.example.mobilephone1.model.MobileModel

interface MobileItemClickListener {

    fun onItemClick(mobileModel: MobileModel)
    fun onFavoriteClick(mobileModel: MobileModel)

}