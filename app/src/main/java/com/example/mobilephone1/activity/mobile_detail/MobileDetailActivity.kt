package com.example.mobilephone1.activity.mobile_detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mobilephone1.R
import com.example.mobilephone1.activity.mobile_detail.mobile_detail_adapter.MobileDetailAdapter
import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.model.MobilePictureModel
import com.example.mobilephone1.service.MobileManager
import kotlinx.android.synthetic.main.mobile_detail.*

class MobileDetailActivity : AppCompatActivity(), MobileDetailInterface {

    private val presenter = MobileDetailPresenter(this, MobileManager().createService())
    private lateinit var mobileDetailAdapter: MobileDetailAdapter

    companion object {
        const val EXTRA_KEY_MODEL = "MODEL"

        fun startActivity(context: Context?, model: MobileModel) =
            context?.startActivity(
                Intent(context, MobileDetailActivity::class.java).also { intent ->
                    intent.putExtra(EXTRA_KEY_MODEL, model)

                }
            )

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mobile_detail)
        presenter.getMobilePicApi(intent.getParcelableExtra(EXTRA_KEY_MODEL))
        presenter.checkMobile(intent.getParcelableExtra(EXTRA_KEY_MODEL))

        mobileDetailAdapter = MobileDetailAdapter()
        rvMobileDetailPic.adapter = mobileDetailAdapter
        rvMobileDetailPic.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

    }

    override fun setMobile(mobileName: String, mobileBrand: String, mobileDes: String) {
        tvName.text = mobileName
        tvbrand.text = mobileBrand
        tvDesc.text = mobileDes

    }

    override fun setMobilePic(mobilePic: List<MobilePictureModel>) {
        mobileDetailAdapter.submit(mobilePic)

    }
    override fun showMessage(msg: String) {

    }


}

