package com.example.mobilephone1.activity.main

import com.example.mobilephone1.model.FragmentModel

interface MainInterface {
    fun setFragment(fragmentList: List<FragmentModel>)
}