package com.example.mobilephone1.activity.mobile_list

import com.example.mobilephone1.model.MobileModel

interface MobileListInterface {

    fun setMobileList(mobileModelList: List<MobileModel>)

    fun updateFavoriteModel(mobileModel: MobileModel)

    fun showMessage(msg: String)
}