package com.example.mobilephone1.activity.favorite

import com.example.mobilephone1.model.MobileModel

interface FavoriteListInterface {
    fun setFavorite(mobileModelList: List<MobileModel>)

}