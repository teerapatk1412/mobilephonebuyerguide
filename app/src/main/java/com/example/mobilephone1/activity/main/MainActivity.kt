package com.example.mobilephone1.activity.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.mobilephone1.R
import com.example.mobilephone1.activity.favorite.FavoriteListFragment
import com.example.mobilephone1.activity.mobile_list.MobileListFragment
import com.example.mobilephone1.model.FragmentModel
import com.example.mobilephone1.model.MobileModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainInterface {

    private lateinit var toolbar: Toolbar
    private val presenter = MainPresenter(this)
    private lateinit var favoriteListFragment: FavoriteListFragment
    private lateinit var mobileListFragment: MobileListFragment

    companion object {
        const val MOBILE_LIST_FRAGMENT_TAB = "MOBILE LIST"
        const val FAVORITE_LIST_FRAGMENT_TAB = "FAVORITE LIST"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.getFragment()
        toolbar = findViewById(R.id.appBarLayout)
        setSupportActionBar(toolbar)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.low_to_high -> {
                mobileListFragment.mobileSortingLowToHigh()
                favoriteListFragment.favoriteSortingLowToHigh()
                true
            }
            R.id.high_to_low -> {
                mobileListFragment.mobileSortingHighToLow()
                favoriteListFragment.favoriteSortingHighToLow()
                true
            }
            R.id.rating_5_to_1 -> {
                mobileListFragment.mobileSortingRating()
                favoriteListFragment.favoriteSortingRating()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun setFragment(fragmentList: List<FragmentModel>) {
        mobileListFragment = fragmentList[0].fragment as MobileListFragment
        favoriteListFragment = fragmentList[1].fragment as FavoriteListFragment
        val sectionPagerAdapter = MainAdapter(fragmentList, supportFragmentManager)
        viewPager.adapter = sectionPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }

    fun sendDataToFavorite(model: MobileModel) {
        favoriteListFragment.checkDataToFavorite(model)
    }


}
