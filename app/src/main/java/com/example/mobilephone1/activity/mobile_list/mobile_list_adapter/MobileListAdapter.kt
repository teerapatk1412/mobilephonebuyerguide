package com.example.mobilephone1.activity.mobile_list.mobile_list_adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilephone1.activity.mobile_list.MobileItemClickListener
import com.example.mobilephone1.model.MobileModel

class MobileListAdapter(private val listener: MobileItemClickListener): RecyclerView.Adapter<MobileListHolder>() {

    private var mobileList: List<MobileModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MobileListHolder {
           return MobileListHolder(parent)
    }

    override fun getItemCount(): Int = mobileList.count()

    override fun onBindViewHolder(holder: MobileListHolder, position: Int) {
        holder.bind(mobileList[position], listener)
    }

    fun submitList(list: List<MobileModel>){
        mobileList = list
        notifyDataSetChanged()
    }




}