package com.example.mobilephone1.activity.mobile_list

import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.service.MobileApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileListPresenter(val view: MobileListInterface, private val service: MobileApiService) {

    private var mobileList: List<MobileModel> = listOf()

    fun getMobileApi() {
        service.getMobile().enqueue(object : Callback<List<MobileModel>> {
            override fun onFailure(call: Call<List<MobileModel>>, t: Throwable) {
//                Log.i("getApiFail: ", t.message.toString())
                view.showMessage("Api Failed")

            }

            override fun onResponse(call: Call<List<MobileModel>>, response: Response<List<MobileModel>>) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        mobileList = this
                        view.setMobileList(mobileList)
                    }
                }
            }
        })
    }

    fun changeFavorite(mobileModel: MobileModel) {
        mobileModel.favorite = !mobileModel.favorite
        view.setMobileList(mobileList)
        view.updateFavoriteModel(mobileModel)
    }

    fun mobileSortLowToHigh() {
        mobileList = mobileList.sortedBy { it.price }
        view.setMobileList(mobileList)
    }

    fun mobileSortHighToLow() {
        mobileList = mobileList.sortedByDescending { it.price }
        view.setMobileList(mobileList)
    }

    fun mobileSortRating() {
        mobileList = mobileList.sortedByDescending { it.rating }
        view.setMobileList(mobileList)
    }
}