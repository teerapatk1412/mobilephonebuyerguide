package com.example.mobilephone1.activity.favorite

import com.example.mobilephone1.model.MobileModel

class FavoriteListPresenter(val view: FavoriteListInterface) {

    private var favoriteMobileList: List<MobileModel> = listOf()

    fun setData(model: MobileModel) {
        favoriteMobileList = if (model.favorite) {
            favoriteMobileList.plus(model)
        }else{
            favoriteMobileList.minus(model)
        }
        view.setFavorite(favoriteMobileList)
    }

    fun favoriteSortLowToHigh() {
        favoriteMobileList = favoriteMobileList.sortedBy { it.price }
        view.setFavorite(favoriteMobileList)

    }

    fun favoriteSortHighToLow() {
        favoriteMobileList = favoriteMobileList.sortedByDescending { it.price }
        view.setFavorite(favoriteMobileList)

    }

    fun favoriteSortRating() {
        favoriteMobileList = favoriteMobileList.sortedByDescending { it.rating }
        view.setFavorite(favoriteMobileList)

    }

}