package com.example.mobilephone1.activity.mobile_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mobilephone1.R
import com.example.mobilephone1.activity.main.MainActivity
import com.example.mobilephone1.activity.mobile_detail.MobileDetailActivity
import com.example.mobilephone1.activity.mobile_list.mobile_list_adapter.MobileListAdapter
import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.service.MobileManager
import kotlinx.android.synthetic.main.mobilelist.*

class MobileListFragment : Fragment(), MobileListInterface {

    private val presenter = MobileListPresenter(this, MobileManager().createService())
    private lateinit var mobileListAdapter: MobileListAdapter

    override fun setMobileList(mobileModelList: List<MobileModel>) {
        mobileListAdapter.submitList(mobileModelList)
    }

    companion object {
        fun newInstance(): MobileListFragment = MobileListFragment()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.mobilelist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialAdapter()
        presenter.getMobileApi()
    }

    private fun initialAdapter() {
        val listener = object : MobileItemClickListener {
            override fun onFavoriteClick(mobileModel: MobileModel) {
                presenter.changeFavorite(mobileModel)
            }

            override fun onItemClick(mobileModel: MobileModel) {
                MobileDetailActivity.startActivity(context, mobileModel)
            }
        }
        mobileListAdapter = MobileListAdapter(listener)
        rvMobileList.adapter = mobileListAdapter
        rvMobileList.layoutManager = LinearLayoutManager(context)
    }

    override fun updateFavoriteModel(mobileModel: MobileModel) {
        getParentActivity()?.sendDataToFavorite(mobileModel)
    }

    private fun getParentActivity() = activity as? MainActivity

    fun mobileSortingLowToHigh() {
        presenter.mobileSortLowToHigh()
    }

    fun mobileSortingHighToLow() {
        presenter.mobileSortHighToLow()
    }

    fun mobileSortingRating() {
        presenter.mobileSortRating()
    }
    override fun showMessage(msg: String) {

    }

}