package com.example.mobilephone1.activity.main

import com.example.mobilephone1.model.FragmentModel
import com.example.mobilephone1.activity.favorite.FavoriteListFragment
import com.example.mobilephone1.activity.mobile_list.MobileListFragment

class MainPresenter(val view: MainInterface) {

    fun getFragment() {
        val tabList = listOf(
                FragmentModel(MainActivity.MOBILE_LIST_FRAGMENT_TAB, MobileListFragment.newInstance()),
                FragmentModel(MainActivity.FAVORITE_LIST_FRAGMENT_TAB, FavoriteListFragment.newInstance()))

        view.setFragment(tabList)
    }

}