package com.example.mobilephone1.activity.favorite.favorite_adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mobilephone1.model.MobileModel

class FavoriteListAdapter : RecyclerView.Adapter<FavoriteListHolder>() {

    private var favoriteModel: List<MobileModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteListHolder {
        return FavoriteListHolder(parent)
    }

    override fun getItemCount(): Int = favoriteModel.count()

    override fun onBindViewHolder(holder: FavoriteListHolder, position: Int) {
        holder.bind(favoriteModel[position])
    }

    fun submit(list: List<MobileModel>) {
        favoriteModel = list
        notifyDataSetChanged()
    }


}