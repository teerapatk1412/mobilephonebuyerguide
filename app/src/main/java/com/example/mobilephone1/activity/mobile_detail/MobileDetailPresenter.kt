package com.example.mobilephone1.activity.mobile_detail

import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.model.MobilePictureModel
import com.example.mobilephone1.service.MobileApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileDetailPresenter(private val view: MobileDetailInterface, private val service: MobileApiService) {

    private var mobilePictureModel: List<MobilePictureModel> = listOf()

    fun checkMobile(mobileItem: MobileModel) {
        view.setMobile(
            "${mobileItem.name}",
            mobileItem.brand ?: "",
            mobileItem.description ?: ""
        )

    }

    fun getMobilePicApi(mobileItem: MobileModel) {
        service.getPictureMobile(mobileItem.id).enqueue(object : Callback<List<MobilePictureModel>> {
            override fun onFailure(call: Call<List<MobilePictureModel>>, t: Throwable) {
                view.showMessage("Api Failed")
            }

            override fun onResponse(
                call: Call<List<MobilePictureModel>>,
                response: Response<List<MobilePictureModel>>
            ) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        mobilePictureModel = this
                        view.setMobilePic(mobilePictureModel)
                    }
                }
            }

        })
    }


}