package com.example.mobilephone1.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobilePictureModel (
    @SerializedName("url")
    val url: String?,

    @SerializedName("mobileId")
    val mobileId: Int?,

    @SerializedName("id")
    val id: Int?

) : Parcelable
