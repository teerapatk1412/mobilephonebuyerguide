package com.example.mobilephone1.model

import androidx.fragment.app.Fragment

class FragmentModel (
    val tabName: String,
    val fragment: Fragment
)