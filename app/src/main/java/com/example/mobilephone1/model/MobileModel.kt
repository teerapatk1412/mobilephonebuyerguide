package com.example.mobilephone1.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobileModel(
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("rating")
    val rating: String?,

    @SerializedName("price")
    val price: String?,

    @SerializedName("brand")
    val brand: String?,

    @SerializedName("thumbImageURL")
    val thumbImageURL: String?,

    @SerializedName("favorite")
    var favorite : Boolean = false

) : Parcelable

