package com.example.mobilephone1.activity.mobile_detail

import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.model.MobilePictureModel
import com.example.mobilephone1.service.MobileApiService
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileDetailPresenterTest {
    @Mock
    private lateinit var view: MobileDetailInterface

    @Mock
    private lateinit var service: MobileApiService

    @InjectMocks
    private lateinit var presenter: MobileDetailPresenter

    private lateinit var mobileItem: MobileModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mobileItem = MobileModel(
            1, "name", "des", "1.0", "1.0",
            "brand", "thumblmageURL", false
        )
    }

    @Test
    fun testGetMobilePictureApi() {
        whenever(service.getPictureMobile(mobileItem.id)).thenReturn(mock())

        presenter.getMobilePicApi(mobileItem)

        verify(service).getPictureMobile(mobileItem.id)
    }

    @Test
    fun testGetMobilePictureApiFailed() {
        val call = mock<Call<List<MobilePictureModel>>>()
        whenever(service.getPictureMobile(mobileItem.id)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobilePictureModel>>>(0).onFailure(mock(), mock())
        }

        presenter.getMobilePicApi(mobileItem)

        verify(view).showMessage("Api Failed")
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testGetMobilePictureApiResponseBodyNull() {
        val call = mock<Call<List<MobilePictureModel>>>()
        whenever(service.getPictureMobile(mobileItem.id)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobilePictureModel>>>(0).onResponse(mock(), Response.success(null))
        }

        presenter.getMobilePicApi(mobileItem)

        verifyZeroInteractions(view)
    }

    @Test
    fun testGetMobilePictureApiResponseBodyEmpty() {
        val call = mock<Call<List<MobilePictureModel>>>()
        whenever(service.getPictureMobile(mobileItem.id)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobilePictureModel>>>(0).onResponse(mock(), Response.success(listOf()))
        }

        presenter.getMobilePicApi(mobileItem)

        verifyZeroInteractions(view)
    }

    @Test
    fun testGetMobilePictureApiResponseBodyMobilePictureModel() {
        val mobilePictureModel = MobilePictureModel("url", 1, 1)
        val call = mock<Call<List<MobilePictureModel>>>()
        whenever(service.getPictureMobile(mobileItem.id)).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobilePictureModel>>>(0)
                .onResponse(mock(), Response.success(listOf(mobilePictureModel)))
        }

        presenter.getMobilePicApi(mobileItem)

        verify(view).setMobilePic(eq(listOf(mobilePictureModel)))
    }

    @Test
    fun testCheckMobile() {
        presenter.checkMobile(mobileItem)

        verify(view).setMobile("name", "brand", "des")
    }

}