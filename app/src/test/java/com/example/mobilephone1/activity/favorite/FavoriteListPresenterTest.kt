package com.example.mobilephone1.activity.favorite

import com.example.mobilephone1.model.MobileModel
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.verify
import mockit.Deencapsulation
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations


class FavoriteListPresenterTest {
    @Mock
    private lateinit var view: FavoriteListInterface

    @InjectMocks
    private lateinit var presenter: FavoriteListPresenter

    private lateinit var listMobile: List<MobileModel>
    private lateinit var mobileModel1: MobileModel
    private lateinit var mobileModel2: MobileModel
    private lateinit var mobileModel3: MobileModel

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        mobileModel1 = MobileModel(1,"name","des", "3.0",
            "1.0", "brand", "url", false)
        mobileModel2 = MobileModel(2,"name1","des1", "1.0",
            "2.0", "brand1", "url1", false)
        mobileModel3 = MobileModel(3,"name2","des2", "5.0",
            "3.0", "brand2", "url2", false)
        listMobile = listOf(mobileModel2, mobileModel1, mobileModel3)
    }
    @Test
    fun testSetDataAdd(){
        val mobileModel1 = MobileModel(1,"name","des", "3.0",
            "1.0", "brand", "url", true)
        val mobileModel2 = MobileModel(2,"name1","des1", "1.0",
            "2.0", "brand1", "url1", true)
        val listMobile = listOf((mobileModel1))
        val exList = listOf(mobileModel1, mobileModel2)
        Deencapsulation.setField(presenter, "favoriteMobileList", listMobile)

        presenter.setData(mobileModel2)

        verify(view).setFavorite(eq(exList))
    }
    @Test
    fun testSetDataDelete(){
        val mobileModel1 = MobileModel(1,"name","des", "3.0",
            "1.0", "brand", "url", true)
        val mobileModel2 = MobileModel(2,"name1","des1", "1.0",
            "2.0", "brand1", "url1", false)
        val listMobile = listOf(mobileModel1, mobileModel2)
        val exList = listOf(mobileModel1)
        Deencapsulation.setField(presenter, "favoriteMobileList", listMobile)

        presenter.setData(mobileModel2)

        verify(view).setFavorite(eq(exList))
    }
    @Test
    fun testFavoriteLowToHigh(){
        val exList = listOf(mobileModel1, mobileModel2, mobileModel3)
        Deencapsulation.setField(presenter, "favoriteMobileList", listMobile)

        presenter.favoriteSortLowToHigh()

        verify(view).setFavorite(eq(exList))
    }
    @Test
    fun testFavoriteHighToLow(){
        val exList = listOf(mobileModel3, mobileModel2, mobileModel1)
        Deencapsulation.setField(presenter, "favoriteMobileList", listMobile)

        presenter.favoriteSortHighToLow()

        verify(view).setFavorite(eq(exList))
    }
    @Test
    fun testFavoriteSortRating(){
        val exList = listOf(mobileModel3, mobileModel1, mobileModel2)
        Deencapsulation.setField(presenter, "favoriteMobileList", listMobile)

        presenter.favoriteSortRating()

        verify(view).setFavorite(eq(exList))
    }

}