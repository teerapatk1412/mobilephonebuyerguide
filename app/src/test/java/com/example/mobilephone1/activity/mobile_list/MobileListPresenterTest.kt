package com.example.mobilephone1.activity.mobile_list

import com.example.mobilephone1.model.MobileModel
import com.example.mobilephone1.service.MobileApiService
import com.nhaarman.mockitokotlin2.*
import mockit.Deencapsulation
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MobileListPresenterTest {

    @Mock
    private lateinit var view: MobileListInterface

    @Mock
    private lateinit var service: MobileApiService

    @InjectMocks
    private lateinit var presenter: MobileListPresenter

    private lateinit var listMobile: List<MobileModel>
    private lateinit var mobileModel1: MobileModel
    private lateinit var mobileModel2: MobileModel
    private lateinit var mobileModel3: MobileModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mobileModel1 = MobileModel(
            1, "name", "des", "3.0",
            "1.0", "brand", "url", false
        )
        mobileModel2 = MobileModel(
            2, "name1", "des1", "1.0",
            "2.0", "brand1", "url1", false
        )
        mobileModel3 = MobileModel(
            3, "name2", "des2", "5.0",
            "3.0", "brand2", "url2", false
        )
        listMobile = listOf(mobileModel2, mobileModel1, mobileModel3)
    }

    @Test
    fun testGetMobileApi() {
        whenever(service.getMobile()).thenReturn(mock())

        presenter.getMobileApi()

        verify(service).getMobile()
    }

    @Test
    fun testGetMobileApiFailed() {
        val call = mock<Call<List<MobileModel>>>()
        whenever(service.getMobile()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobileModel>>>(0).onFailure(mock(), mock())
        }

        presenter.getMobileApi()

        verify(view).showMessage("Api Failed")
        verifyNoMoreInteractions(view)
    }

    @Test
    fun testGetMobileApiResponseBodyNull() {
        val call = mock<Call<List<MobileModel>>>()
        whenever(service.getMobile()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobileModel>>>(0).onResponse(mock(), Response.success(null))
        }

        presenter.getMobileApi()

        verifyZeroInteractions(view)
    }

    @Test
    fun testGetMobileApiResponseBodyEmpty() {
        val call = mock<Call<List<MobileModel>>>()
        whenever(service.getMobile()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobileModel>>>(0).onResponse(mock(), Response.success(listOf()))
        }

        presenter.getMobileApi()

        verifyZeroInteractions(view)
    }

    @Test
    fun testGetMobileApiResponseBodyMobileModel() {
        val mobileModel = MobileModel(
            1, "name", "des", "1.0",
            "1.0", "brand", "url", false
        )
        val call = mock<Call<List<MobileModel>>>()
        whenever(service.getMobile()).thenReturn(call)
        whenever(call.enqueue(any())).thenAnswer {
            it.getArgument<Callback<List<MobileModel>>>(0).onResponse(mock(), Response.success(listOf(mobileModel)))
        }

        presenter.getMobileApi()

        verify(view).setMobileList(any())
    }

    @Test
    fun testChangeFavorite() {
        val mobileModel = MobileModel(
            1, "name", "des", "1.0",
            "1.0", "brand", "url", false
        )
        Deencapsulation.setField(presenter, "mobileList", listOf(mobileModel))

        presenter.changeFavorite(mobileModel)

        verify(view).setMobileList(eq(listOf(mobileModel)))
        verify(view).updateFavoriteModel(eq(mobileModel))
        assertEquals(true, mobileModel.favorite)
    }

    @Test
    fun testMobileSortLowToHigh() {
        val exList = listOf(mobileModel1, mobileModel2, mobileModel3)
        Deencapsulation.setField(presenter, "mobileList", listMobile)

        presenter.mobileSortLowToHigh()

        verify(view).setMobileList(eq(exList))
    }

    @Test
    fun testMobileSortHighTolow() {
        val exList = listOf(mobileModel3, mobileModel2, mobileModel1)
        Deencapsulation.setField(presenter, "mobileList", listMobile)

        presenter.mobileSortHighToLow()

        verify(view).setMobileList(eq(exList))
    }

    @Test
    fun testMobileSortRating() {
        val exList = listOf(mobileModel3, mobileModel1, mobileModel2)
        Deencapsulation.setField(presenter, "mobileList", listMobile)

        presenter.mobileSortRating()

        verify(view).setMobileList(eq(exList))
    }
}