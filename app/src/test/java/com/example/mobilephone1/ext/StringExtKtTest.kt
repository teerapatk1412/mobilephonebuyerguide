package com.example.mobilephone1.ext

import junit.framework.TestCase.assertEquals
import org.junit.Test

class StringExtKtTest {

    @Test
    fun testStringNotHttp() {
        val url = "www.google.com"
        val urlExt = "https://www.google.com"

        val result = url.validHttp()

        assertEquals(result, urlExt)
    }

    @Test
    fun testStringHttp() {
        val url = "http://www.google.com"
        val urlExt = "http://www.google.com"

        val result = url.validHttp()

        assertEquals(result, urlExt)
    }

    @Test
    fun testStringHttps() {
        val url = "https://www.google.com"
        val urlExt = "https://www.google.com"

        val result = url.validHttp()

        assertEquals(result, urlExt)
    }

    @Test
    fun testStringEmpty() {
        val url = ""
        val urlExt = "https://"

        val result = url.validHttp()

        assertEquals(result, urlExt)
    }
}